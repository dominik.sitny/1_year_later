//
//  MyGroups.swift
//  1_Year_LaterTests
//
//  Created by Dominik Sitny on 26.02.23.
//

import XCTest

class MyTests: XCTestCase {
    
    func testDaysBetween() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startDate = formatter.date(from: "2022-01-01")!
        let endDate = formatter.date(from: "2022-12-31")!
        let result = AllFunctions().daysBetween(end: endDate, start: startDate)
        print(result)
        XCTAssertEqual(result, 1, "Fehler beim Berechnen der Anzahl von Tagen zwischen den Daten")
    }
        
        func testGetViewFriends() {
            // Arrange
            let user1 = User(id: "1", name: "Alice", username:"Alice1", admin:false)
            let user2 = User(id: "2", name: "Bob", username: "Bob1", admin:false)
            let user3 = User(id: "3", name: "Charlie", username: "Charlie2", admin: false)
            let users = [user1, user2, user3]
            
            let group = Group(id: "1", name: "Test Group", memberList: [user1, user3], erstellungsdatum: Date())
            
            // Act
            let viewFriends = AllFunctions().getViewFriends(group: group, users: users)
            
            // Assert
            XCTAssertEqual(viewFriends.count, 1, "Die Anzahl der Freunde, die angezeigt werden können, ist nicht korrekt.")
        }
        
        let allUsers = ["user1", "user2", "user3"]
        
        func testCheckUsername_UsernameExists_ReturnsZero() {
            // Arrange
            let username = "user1"
            
            // Act
            let result = AllFunctions().checkUsername(text: username, alleUser: allUsers)
            
            // Assert
            XCTAssertEqual(result, 0)
        }
        
        func testCheckUsername_UsernameTooShort_ReturnsOne() {
            // Arrange
            let username = "user"
            
            // Act
            let result = AllFunctions().checkUsername(text: username, alleUser: allUsers)
            
            // Assert
            XCTAssertEqual(result, 1)
        }
        
        func testCheckUsername_UsernameIsValid_ReturnsTwo() {
            // Arrange
            let username = "newuser"
            
            // Act
            let result = AllFunctions().checkUsername(text: username, alleUser: allUsers)
            
            // Assert
            XCTAssertEqual(result, 2)
        }
        
        func testCheckBothPasswords_PasswordsMatch_ReturnsTrue() {
            // Arrange
            let password1 = "password"
            let password2 = "password"
            
            // Act
            let result = AllFunctions().checkBothPasswords(psw1: password1, psw2: password2)
            
            // Assert
            XCTAssertTrue(result)
        }
        
        func testCheckBothPasswords_PasswordsDoNotMatch_ReturnsFalse() {
            // Arrange
            let password1 = "password"
            let password2 = "password123"
            
            // Act
            let result = AllFunctions().checkBothPasswords(psw1: password1, psw2: password2)
            
            // Assert
            XCTAssertFalse(result)
        }
        
        func testCheckLengthPassword_PasswordIsTooShort_ReturnsFalse() {
            // Arrange
            let password = "passw"
            
            // Act
            let result = AllFunctions().checkLengthPassword(psw1: password)
            
            // Assert
            XCTAssertFalse(result)
        }
        
        func testCheckLengthPassword_PasswordIsValid_ReturnsTrue() {
            // Arrange
            let password = "password"
            
            // Act
            let result = AllFunctions().checkLengthPassword(psw1: password)
            
            // Assert
            XCTAssertTrue(result)
        }
    

    
}
