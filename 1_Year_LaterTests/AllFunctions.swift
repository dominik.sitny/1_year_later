//
//  AllFunctions.swift
//  1_Year_LaterTests
//
//  Created by Dominik Sitny on 26.02.23.
//

import Foundation


class AllFunctions {
    
    func daysBetween(end: Date, start: Date) -> Int {
        return 365 - Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    func getViewFriends(group:Group, users:[User]) -> [User] {
        
        var friendsId:[String] = []
        var membersId:[String] = []
        var viewFriends:[User] = []
        
        
        for friend in users {
            friendsId.append(friend.id)
        }
        for member in group.memberList{
            membersId.append(member.id)
        }
        for id in friendsId {
            if membersId.contains(id) {
            }
            else{
                
                for user in users{
                    if id == user.id{
                        viewFriends.append(user)
                    }
                }
                }
            }
        return viewFriends
    }
    func checkUsername(text : String, alleUser:[String]) -> Int {
        
        for benutzer in alleUser{
            if(benutzer==text){
                return 0;
            }
        }
        
        if text.count<5{
            return 1
        }
        else{
            return 2
        }
    }

    func checkBothPasswords(psw1:String, psw2:String) -> Bool{
        if(psw1 == psw2){
                return true
        }else{
            return false
        }
    }

    func checkLengthPassword(psw1:String) -> Bool {
        
        if psw1.count<6{
            return false
        }
        else{
            return true
        }
    }
}

struct User: Identifiable, Codable {
    
    var id: String
    var name:String
    var username:String
    var password:String?
    var admin:Bool
    var friendslist:[String]?
    var state:[Int]?
}

struct Group: Identifiable, Codable {
       
    var id: String
    var name: String
    var memberList: [User]
    var erstellungsdatum:Date
    
}
