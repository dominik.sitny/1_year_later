//
//  GruppenController.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 23.02.23.
//

import Foundation

class GruppenController{
    
    private var group:Group
    
    init(group: Group) {
        self.group = group
    }
    
    
    /**
        Hier wird eine neue Gruppe angelegt
     */
    func saveNewGroup(){
        
        let url = URL(string: Configs().restAdresse+"/newGroup")!
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        encoder.dateEncodingStrategy = .iso8601

        let jsonData = try! encoder.encode(group)

        var request = URLRequest(url: url)
        request.httpMethod = "post"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error:", error)
                return
            }
        }

        task.resume()
        
    }
    
    /**
            User verlässt die Gruppe
     */
    
    func leaveGroup(userId:String){
        let url = URL(string: Configs.init().restAdresse+"/leaveGroup")
        guard let requestUrl = url else { fatalError() }

        // Prepare URL Request Object
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "Post"
         
        // HTTP Request Parameters which will be sent in HTTP Request Body

        let postString = "userId="+userId+"&groupId="+group.id

        // Set HTTP Request Body
        request.httpBody = postString.data(using: String.Encoding.utf8);

        // Perform HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
         
                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                }
        }
        task.resume()
    }

    /**
     Admin löscht die Gruppe
     */
    func deleteGroup() -> Void {
        // Prepare URL
        let url = URL(string: Configs.init().restAdresse+"/deleteGroup/"+group.id)
        guard let requestUrl = url else { fatalError() }

        // Prepare URL Request Object
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "DELETE"

        // Perform HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
         
                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                }
        }
        task.resume()
    
    }
    
    /**
        Admin fügt neue Teilnehmer in die Gruppe hinzu
     */
    func addFriendsToGroup(listIds:Set<String>){
        let url = URL(string: Configs().restAdresse+"/addFriendsToGroup/"+group.id)!
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        let jsonData = try! encoder.encode(listIds)
        
        var request = URLRequest(url: url)
        request.httpMethod = "post"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error:", error)
                return
            }
        }
        
        task.resume()
    }
    
    /**
        Admin entfernt Teilnehmer von Gruppe
     */
    func removeFriends(user:User){
        let url = URL(string: Configs().restAdresse+"/removeUserFromGroup/"+group.id)!
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        let jsonData = try! encoder.encode(user)
        
        var request = URLRequest(url: url)
        request.httpMethod = "post"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error:", error)
                return
            }
        }
        task.resume()
    }
}
