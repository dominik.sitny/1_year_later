//
//  BenutzerController.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 27.01.23.
//

import Foundation

class BenutzerController {
    
    private var benutzername:String
    private var passwort:String
    private var name:String
    
    init(benutzername1 : String, passwort1 : String, name1:String){
        self.benutzername = benutzername1;
        self.passwort = passwort1
        self.name = name1
    }
    
    /**
            Hier wird ein Request geschckt, um einen neuen User anzulegen
     */
    func saveNewUser() -> Void {
        
        // Prepare URL
        let url = URL(string: Configs.init().restAdresse+"/newUser")
        guard let requestUrl = url else { fatalError() }

        // Prepare URL Request Object
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
         
        // HTTP Request Parameters which will be sent in HTTP Request Body
        let postString = "benutzername="+benutzername+"&passwort="+passwort+"&name="+name;

        // Set HTTP Request Body
        request.httpBody = postString.data(using: String.Encoding.utf8);

        // Perform HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
         
                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                }
        }
        task.resume()
    
    }

    
}


