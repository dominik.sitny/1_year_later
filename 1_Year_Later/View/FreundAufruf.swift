import SwiftUI

struct FreundAufruf: View {

    @State private var image = UIImage(systemName:"person")!
    
    var freund:User
    @State var benutzerAngemeldet:User
    
    init(b1:User, bAngemeldet:User){
        self.freund = b1
        self.benutzerAngemeldet = bAngemeldet
    }
    
        var body: some View {
            
            VStack{
                Spacer()
                Image(uiImage: self.image)
                    .resizable()
                    .frame(width: 100, height:100)
                    .cornerRadius(50)
                HStack{
                    Text("Benutzername: ")
                    Text(freund.username)
                        .bold()
                }.frame(height: 50)
                HStack{
                    Text("Name: ")
                    Text(freund.name)
                        .bold()
                }.frame(height: 50)
                
                
                if benutzerAngemeldet.id == freund.id{
                    
                }
                
                else if isFriend(b1: benutzerAngemeldet, bFreunde: freund) == -1{
                    
                    HStack{
                        Text("Freundschaftsstatus: ")
                        Text("nicht befreundet")
                            .bold()
                    }.frame(height: 50)
                    
                    Spacer()
                    
                    Button("Freundschaftsanfrage schicken") {
                        sendFR(b1:benutzerAngemeldet,friend:freund, val: 0)
                            }
                            .buttonStyle(GrowingButton())
                }
                
                else if isFriend(b1: benutzerAngemeldet, bFreunde: freund) == 0{
                    HStack{
                        Text("Freundschaftsstatus: ")
                        Text("wartend")
                            .bold()
                    }.frame(height: 50)
                    
                    Spacer()
                    
                    Button("Freundschaftsanfrage löschen") {
                        sendFR(b1:benutzerAngemeldet,friend:freund, val: 1)
                            }
                            .buttonStyle(GrowingButton())
                }
                else if isFriend(b1: benutzerAngemeldet, bFreunde: freund) == 1{
                    HStack{
                        Text("Freundschaftsstatus: ")
                        Text("wartend")
                            .bold()
                    }.frame(height: 50)
                    
                    Spacer()
                    
                    Button("Freundschaftsanfrage akzeptieren") {
                        print("huhu")
                        sendFR(b1:benutzerAngemeldet,friend:freund, val: 2)
                            }
                            .buttonStyle(GrowingButton())
                }
                
                else if isFriend(b1: benutzerAngemeldet, bFreunde: freund) == 2{
                    HStack{
                        Text("Freunschaftsstatus: ")
                        Text("befreundet")
                            .bold()
                    }.frame(height: 50)
                    
                    Spacer()
                    
                    Button("Freund entfernen") {
                        sendFR(b1:benutzerAngemeldet,friend:freund, val: -1)
                            }
                            .buttonStyle(GrowingButton())
                }
                else{
                    
                }
                
                Spacer()
            }.refreshable {
              
            }
            
        }
    
}
struct FreundAufruf_Previews: PreviewProvider {
    static var previews: some View {
        FreundAufruf(b1: User(id: "63ce2e1279caeb46d4d26584", name: "Oliver Wiesner", username: "OliverWiesner", admin: false, friendslist:["45673849506","2134567890876", "63ce2e1279caeb46d4d26584"], state: [2,2,0]), bAngemeldet:User(id: "63bc9cc861531e716e5b99ef", name: "Dominik Sitny", username: "DominikSitny", admin: false, friendslist: ["63ce2e1279caeb46d4d26584", "234", "12"], state: [0,1,2]))
    }
}

struct GrowingButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(.black)
            .foregroundColor(.white)
            .clipShape(Capsule())
            .scaleEffect(configuration.isPressed ? 1.2 : 1)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}

func isFriend(b1:User, bFreunde:User) -> Int {
    
    var status:Int = -1
    
    b1.friendslist?.indices.forEach { index1 in

            
        if(b1.friendslist![index1] == bFreunde.id){
            status = (b1.state![index1])
                print("status: ")
                print(status)
            }
        }
    return status
}

func sendFR(b1:User, friend:User, val:Int) -> Void {
    
    let fullURL = URL(string: Configs().restAdresse+"/changeFS/"+b1.id)!

    var request = URLRequest(url: fullURL)
    request.httpMethod = "PUT"
    request.allHTTPHeaderFields = [
        "Content-Type": "application/json",
        "Accept": "application/json"
    ]
    let jsonDictionary: [String:Any] = [
        "id": friend.id,
        "state": [val]
    ]

    let data = try! JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
    
    URLSession.shared.uploadTask(with: request, from: data) { (responseData, response, error) in
        if let error = error {
            print("Error making PUT request: \(error.localizedDescription)")
            return
        }
        
        if let responseCode = (response as? HTTPURLResponse)?.statusCode, let responseData = responseData {
            guard responseCode == 200 else {
                print("Invalid response code: \(responseCode)")
                return
            }
            if let responseJSONData = try? JSONSerialization.jsonObject(with: responseData, options: .allowFragments) {
                print("Response JSON data = \(responseJSONData)")
            }
        }
    }.resume()
}
