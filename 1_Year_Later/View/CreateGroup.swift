//
//  CreateGroup.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 04.12.22.
//


import SwiftUI

struct CreateGroup: View {
    
    @State private var b1:User
    @State private var isShowPhotoLibray = false
    @State private var image = UIImage(systemName:"person")!
    @State private var name = ""
    @StateObject var alleFreunde = BenutzerModel()
    @State private var selection = Set<String>()
    
    
    init(b1:User){
        self.b1 = b1
    }
    
    var body: some View {

        VStack{
            Button(action:{
                self.isShowPhotoLibray=true
            }){
                Image(uiImage: self.image)
                    .resizable()
                    .frame(width: 100, height:100)
                    .cornerRadius(50)
            }
            .sheet(isPresented:$isShowPhotoLibray){
                ImagePicker(sourceType:.photoLibrary, selectedImage: self.$image)
            }
            Button(action: {
                self.image = UIImage(systemName:"person")!
            }){
                Text("auswählen")
            }
            TextField("Name", text:$name)
                .padding()
            
            
            List(selection: $selection){
                Section(header:(Text("Teilnehmer aus deinen Freunden hinzufügen:"))){
                    ForEach(alleFreunde.alleFriends, id: \.id) { name in
                        Text(name.username)
                    }
                }
            }
            .task{
                    await alleFreunde.getFriends(id: b1.id)
            }
            .toolbar{
                EditButton()
            }
            Button("Gruppe erstellen"){
                
                var group = Group(id: "", name: "", memberList: [User(id: "", name: "", username: "", admin: false)],erstellungsdatum: Date())
                group.memberList = []
                let formatter = DateFormatter()
                formatter.setLocalizedDateFormatFromTemplate("yyyy-MM-dd'T'HH:mm:ss")
                group.erstellungsdatum = formatter.date(from: formatter.string(from: Date()))!
                b1.admin = true
                group.memberList.append(b1)
                for id in selection {
                    let user = User(id: id, name: "", username: "", admin: false)
                    group.memberList.append(user)
                    
                }
                group.name = name
                print(group)
                GruppenController(group: group).saveNewGroup()
            }.buttonStyle(GrowingButton())
        }
        
    }
}

