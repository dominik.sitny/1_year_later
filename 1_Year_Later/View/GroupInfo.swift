//
//  GroupInfo.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 06.12.22.
//

import SwiftUI

struct GroupInfo: View {
    
    var user:User
    var group:Group
    @StateObject var groups:GruppeModel = GruppeModel()
    @State var admin:Int = 0
    @State private var image = UIImage(systemName:"person")!
    @State private var name:String = ""
    
    init(group:Group, b1:User){
        self.user = b1
        self.group = group
    }
    
    var body: some View {
            VStack{
                Spacer()
                Image(uiImage: self.image)
                    .resizable()
                    .frame(width: 100, height:100)
                    .cornerRadius(50)
                Spacer()
                HStack(alignment: .center){
                    
                    Text("Name: ")
                    TextField(group.name, text: $name,onEditingChanged: {_ in
                        
                    })
                    
                }
                .padding()
                List{
                    Section(header: (Text("Teilnehmer"))) {
                        ForEach(group.memberList, id: \.id) { name in
                            NavigationLink {
                                FreundAufruf(b1: name, bAngemeldet: user)
                            }
                        label:{
                            Text(name.username)
                        }
                        }
                        .onDelete(perform: delete)

                        
                      
                    }
                    if self.admin == 1{
                        Section(header: Text("Freund hinzufügen")){
                            NavigationLink{
                                FriendsList(group: group, user: user)
                            } label: {
                                Text("Freunde hinzufügen")
                            }
                        }.scrollDisabled(true)
                    }
                        if self.admin == 1{
                            Button("Gruppe löschen"){
                                GruppenController(group: group).deleteGroup()
                            }.buttonStyle(GrowingButton())
                        }else if self.admin == 2{
                            Button("Gruppe verlassen"){
                                GruppenController(group: group).leaveGroup(userId: user.id)
                            }.buttonStyle(GrowingButton())
                        }
                        else{
                            VStack{
                                ProgressView()
                                Text("Loading")
                            }
                        }
                        
                    
                }.task {
                        isAdmin(userId: user.id, groupId: group.id)
                }
                
                
                .onAppear {
                    self.groups.getGroups(id: group.id, group: true)
                    
                }
                .toolbar{
                    if self.admin == 1 {
                        EditButton()
                    }
                }
        }
        
        .navigationTitle(group.name)
    }
     func delete(at offsets: IndexSet) {
         groups.alleGruppen[0].memberList.remove(atOffsets: offsets)
         let index = offsets[offsets.startIndex]
         GruppenController(group: group).removeFriends(user: group.memberList[index])
       }
    func isAdmin(userId:String, groupId:String) {

        let url = URL(string: Configs.init().restAdresse+"/isAdmin?userId="+userId+"&groupId="+groupId)!
        let urlSession = URLSession.shared
        
            let task = urlSession.dataTask(with: url) { data, _, error in
                // Error handling in case the data couldn't be loaded
                // For now, only display the error on the console
                guard let data = data else {
                    debugPrint("Error loading \(url): \(String(describing: error))")
                    return
                }
               let isAdmin = try! JSONDecoder().decode(Bool.self, from: data)
                if isAdmin {
                    self.admin = 1
                }else{
                    self.admin = 2
                }
            }
            task.resume()
    }
}

struct FriendsList: View {
    
    var group:Group
    var user:User
    @StateObject var alleFriends = BenutzerModel()
    @State private var selection = Set<String>()
    
    init(group:Group, user:User){
        self.group = group
        self.user = user
    }
    
    var body: some View {
        VStack{
                List(selection: $selection) {
                        Section(header: Text("Freunde hinzufügen")){
                            ForEach(getViewFriends(group: group, users: alleFriends.alleFriends), id: \.id){ user in
                                Text(user.name)
                            }
                        }
                }.task {
                    alleFriends.getFriends2(id: user.id)
                }
                .toolbar{
                    EditButton()
                }
            Button("speichern"){
                print(selection)
                if selection == [] {
                    
                }else{
                    GruppenController(group: group).addFriendsToGroup(listIds: selection)
                }
            }
            .buttonStyle(GrowingButton())

        }
    }
}

func getViewFriends(group:Group, users:[User]) -> [User] {
    
    var friendsId:[String] = []
    var membersId:[String] = []
    var viewFriends:[User] = []
    
    
    for friend in users {
        friendsId.append(friend.id)
    }
    for member in group.memberList{
        membersId.append(member.id)
    }
    for id in friendsId {
        if membersId.contains(id) {
        }
        else{
            
            for user in users{
                if id == user.id{
                    viewFriends.append(user)
                }
            }
            }
        }
    return viewFriends
}
