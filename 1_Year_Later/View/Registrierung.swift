//
//  Registrierung.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 07.12.22.
//

import SwiftUI

struct Registrierung: View {
    
    @StateObject var benutzerModel = BenutzerModel()
    @State private var username = ""
    @State private var check = ""
    @State private var passwort = ""
    @State private var passwort2 = ""
    @State private var name = ""
    @State private var counter:Int = 0;
    
    
    var body: some View {
            HStack{
                VStack{
                    Spacer()
                    ZStack(alignment: .center){
                        
                        Image(systemName: "lock.fill")
                            .resizable()
                            .frame(width: 160, height: 250)
                        Text(Configs().appname)
                            .font(.system(size:20))
                            .foregroundColor(Color.white)
                            .padding(.top,100)
                    }
                    Spacer()
                    Text("Registrierung")
                        .font(.system(size:27))
                        .padding()
                        .bold()
                    Spacer()
                    Text(check)
                        .foregroundColor(Color.red)
                    VStack{
                        TextField("Benutzername", text:$username)
                            .padding()
                        Divider()
                        TextField("Name", text:$name)
                            .padding()
                        Divider()
                        TextField("Passwort" ,text:$passwort)
                            .padding()
                        Divider()
                        TextField("Passwort wiederholen" ,text:$passwort2)
                            .padding()
                    }
                    .onAppear{
                        //benutzerModel.getUsernames(url: Configs.init().restAdresse+"/getAllUserNames")
                        let url = URL(string: Configs.init().restAdresse+"/getAllUserNames")!
                        let urlSession = URLSession.shared
                        let task = urlSession.dataTask(with: url) { data, _, error in
                            // Error handling in case the data couldn't be loaded
                            // For now, only display the error on the console
                            guard let data = data else {
                                debugPrint("Error loading \(url): \(String(describing: error))")
                                return
                            }
                            // Parse JSON with JSONDecoder assuming valid JSON data
                            let alleBenutzer = try! JSONDecoder().decode([String].self, from: data)
                            if(alleBenutzer.count==0){
                                
                            }
                            else{
                                DispatchQueue.main.async {
                                    benutzerModel.allUsernames = alleBenutzer
                                    
                                }
                                
                            }
                            // Update UI
                            OperationQueue.main.addOperation {
                                benutzerModel.allUsernames = alleBenutzer
                                
                            }
                        }
                        task.resume()
                    }
                    
                    Button("registrieren"){
                            print(benutzerModel.allUsernames)
                            self.counter = 0
                            if checkBothPasswords(psw1: self.passwort, psw2: self.passwort2){
                                self.counter = self.counter + 1;
                            }
                            else{
                                check = "Die Passwörter stimmen nicht überein"
                                self.counter = 0;
                            }
                            if checkLengthPassword(psw1: self.passwort){
                                self.counter = self.counter + 1;
                            }
                            else{
                                check = "Das Passwort muss mindestens 6 Zeichen lang nutzen"
                                self.counter = 0;
                            }
                            
                            if self.name.count==0{
                                check = "Der Name muss ausgefüllt werden"
                                self.counter = 0;
                            }
                            else{
                                self.counter = self.counter + 1;
                            }
                            
                            if checkUsername(text:self.username, alleUser: benutzerModel.allUsernames) == 2{
                                self.counter = self.counter + 1;
                            }else if (checkUsername(text:self.username, alleUser: benutzerModel.allUsernames) == 1){
                                check = "Der Benutzername muss mindestens 4 Zeichen lang sein";
                                self.counter = 0;
                            }
                            else{
                                check = "Der Benutzername ist leider schon vergeben"
                                self.counter = 0;
                            }
                            if(self.counter==4){
                                check=""
                                BenutzerController(benutzername1: username, passwort1: passwort, name1: name).saveNewUser()
                                
                                let window = UIApplication
                                    .shared
                                    .connectedScenes
                                    .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                                    .first { $0.isKeyWindow }
                                
                                window?.rootViewController = UIHostingController(rootView: LogIn())
                                window?.makeKeyAndVisible()
                            }
                        }
                }
            }
    }

}

struct Registrierung_Previews: PreviewProvider {
    static var previews: some View {
        Registrierung()
    }
}

func checkUsername(text : String, alleUser:[String]) -> Int {
    
    for benutzer in alleUser{
        if(benutzer==text){
            return 0;
        }
    }
    
    if text.count<5{
        return 1
    }
    else{
        return 2
    }
}

func checkBothPasswords(psw1:String, psw2:String) -> Bool{
    if(psw1 == psw2){
            return true
    }else{
        return false
    }
}

func checkLengthPassword(psw1:String) -> Bool {
    
    if psw1.count<6{
        return false
    }
    else{
        return true
    }
}

func getBenutzerName(text:String, text1:String) -> String {
    
    return text;
}
