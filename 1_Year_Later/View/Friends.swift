import SwiftUI

struct Friends1: View {
    
    @State var b1:User
    @State var alleFreunde = BenutzerModel()
    @State var bList:[User]=[]

    init(b1: User) {
        self.b1 = b1
        alleFreunde.getFriends2(id: b1.id)
    }
    
    @State private var searchText = ""
        var body: some View {
            NavigationStack {
                List {
                    ForEach(searchResults, id: \.id) { name in
                        NavigationLink {
                            FreundAufruf(b1: name, bAngemeldet: self.b1)
                        } label: {
                            HStack{
                                Text(name.username)
                                Spacer()
                                if isFriend(b1: b1, bFreunde: name) == 2{
                                    Text("befreundet")
                                        .font(.system(size:13))
                                        .foregroundColor(.green)
                                        .padding()
                                        .bold()
                                }
                                else if isFriend(b1: b1, bFreunde: name) == 1{
                                    Text("akzeptieren")
                                        .font(.system(size:13))
                                        .foregroundColor(.black)
                                        .padding()
                                        .bold()
                                }
                                if isFriend(b1: b1, bFreunde: name) == 0{
                                    Text("wartend")
                                        .font(.system(size:13))
                                        .foregroundColor(.secondary)

                                        .padding()
                                        .bold()
                                }
                            }
                        }
                    }
                }
                .navigationTitle("Suche oder füge Freunde hinzu")
                .task{
                    await alleFreunde.getFriends(id: b1.id)
                }
                
            }.refreshable {
                do {
                        let url = URL(string: Configs.init().restAdresse+"/getUser?id="+b1.id)!
                        let (data, _) = try await URLSession.shared.data(from: url)
                        self.b1 = try JSONDecoder().decode(User.self, from: data)

                                } catch {
                                    print("falsche gelaufen")
                                }
              
                do{
                    await alleFreunde.getFriends(id: b1.id)
                    DispatchQueue.main.async {
                        bList = alleFreunde.alleFriends
                    }
                }
                
            }
            .searchable(text: $searchText)
        }

        var searchResults: [User] {
            if searchText.count<2 {
                return alleFreunde.alleFriends
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                     bList = alleFreunde.getSearchedUsers(name: searchText)
                        }
                
                
                if(bList.count==0){
                    return alleFreunde.alleFriends
                }else{
                    return bList
                }
            }
        }
}
struct Friends_Previews: PreviewProvider {
    static var previews: some View {
    Friends1(b1: User(id: "63bc9cc861531e716e5b99ef", name: "Dominik Sitny", username: "DominikSitny", admin: false))
    }
}

