//
//  ContentView.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 03.12.22.
//

import SwiftUI


struct Menü: View {
    
    var benutzer:User
    
    init(b1:User){
        self.benutzer = b1
        
    }
    
    var body: some View{
    VStack{
        TabView{
            MyGroups(b1: self.benutzer)
                .tabItem{
                    Label("Gruppen", systemImage:"person.3")
                }
            Camera()
                .tabItem{
                    Label("Kamera", systemImage:"camera")
                }
            Friends1(b1: benutzer)
                .tabItem{
                    Label("Freunde", systemImage: "person.badge.plus")
                }
            }
        }
        
    }
}

struct Menue_Previews: PreviewProvider {
    static var previews: some View {
        Menü(b1: User(id: "1", name: "name", username: "benutzername", password: "passwort", admin: false))
    }
}
