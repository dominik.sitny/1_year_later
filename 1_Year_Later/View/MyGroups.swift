//
//  MyGroups.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 04.12.22.
//

import SwiftUI


struct MyGroups: View {
    
    var benutzer:User
    @StateObject var gruppe = GruppeModel()
    @StateObject var bTest = BenutzerModel()
    
    init(b1:User){
        self.benutzer = b1
    }
    
    var body: some View{
        NavigationView{
            VStack{
                List(gruppe.alleGruppen){
                    gruppe2 in
                    NavigationLink(destination:GroupInfo(group:gruppe2, b1: benutzer)){
                            GroupElement(g: gruppe2)
                        }
                        
                    }.onAppear {
                        self.gruppe.getGroups(id: benutzer.id, group: false)
                    }
            }
                    .navigationTitle("Gruppen")
                    .navigationBarItems(//leading:
                    //Label("abbrechen", systemImage: "person")
                    trailing: NavigationLink(destination: CreateGroup(b1: benutzer)) {
                        Label("myImageName", systemImage: "plus"
                        )
                        
                    })
            
                
        }
    }
}

struct MyGroups_Previews: PreviewProvider {
    static var previews: some View {
        Menü(b1: User(id: "1", name: "name", username: "benutzername", password: "passwort", admin: false))
    }
}

struct GroupElement: View {
    
    var g:Group
    
    init(g: Group){
        self.g = g
    }
    
    var body: some View{
        HStack{
            Image(systemName: "person.circle")
                .resizable()
                .frame(width: 50, height:50)
                .foregroundColor(Color.gray)
                
            Spacer()
            
            VStack(alignment:.leading){
                Text(g.name)
                    .font(Font.headline)
                Text("zul. gesendet: vor 3 Std.")
                    //.font(.subheadline)
                    .foregroundColor(Color.secondary)
            }
            Spacer()
            ZStack{
                getImage(group: g)
                    
                                
                getText(group: g)
                    .font(.system(size:8))
                    .foregroundColor(Color.white)
                    .padding(.top, 11)
                    .bold()
                    
            }
        }
        
    }
}
func getText(group:Group) -> Text{
   
    
    let difference = daysBetween(end: Date(), start:group.erstellungsdatum)
    if difference < 1 {
        return Text("")
    }
    else{
        return Text(String(difference))
    }
}

func getImage(group:Group) -> some View{
    
    
    let difference = daysBetween(end: Date(), start:group.erstellungsdatum)
    if difference < 1 {
        
    let image = Image(systemName: "lock.open.fill")
            .resizable()
            .frame(width: 25, height: 25)
            .foregroundColor(Color.green)
    
    return image
    }
    else{
        let image = Image(systemName: "lock.fill")
                .resizable()
                .frame(width: 25, height: 35)
                .foregroundColor(Color.secondary)
        
        return image
    }
}

func daysBetween(end: Date, start: Date) -> Int {
    return 365 - Calendar.current.dateComponents([.day], from: start, to: end).day!
    }

