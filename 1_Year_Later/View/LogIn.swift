//
//  LogIn.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 06.12.22.
//

import SwiftUI
import Auth0
struct LogIn: View {
    
    @StateObject var benutzerModel = BenutzerModel()
    @State var userProfile = Profile.empty
    @State private var username: String = ""
    @State private var passswort: String = ""
    @State private var check:String = ""
    @State private var joke = "";
    
    var body: some View {
        NavigationView{
            HStack{
                Spacer()
                VStack(){
                    Spacer()
                    ZStack(alignment: .center){
                        Image(systemName: "lock.fill")
                            .resizable()
                            .frame(width: 160, height: 250)
                        Text(Configs().appname)
                            .font(.system(size:20))
                            .foregroundColor(Color.white)
                            .padding(.top,100)
                            
                    }
                    Spacer()
                    Text("Login")
                        .font(.system(size:27))
                        .padding()
                        .bold()
                    /** VStack{
                        Text("Anmelden mit")
                            .foregroundColor(.primary)
                            .bold()
                        HStack(){
                            Button {
                                login()
                            } label: {
                                Image(systemName: "apple.logo")
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                    .padding()
                                    .foregroundColor(Color.black)
                            }
                                
                            Image("Google.logo")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .padding()
                        }
                        Text(check)
                            .foregroundColor(Color.red)
                    } */
                    
                    TextField("Benutzername", text:$username)
                        .padding()
                    Divider()
                    TextField("Passwort", text:$passswort)
                        .padding()
                    Divider()
                    HStack{
                        Spacer()
                        NavigationLink(destination: Registrierung()) {
                                Label("Noch kein Account?", systemImage: "")
                                    .foregroundColor(Color.blue
                            )
                        }
                        Spacer()
                        
                        Button(action: {
                                if let url = URL(string: Configs.init().restAdresse+"/LogIn?username="+username+"&password="+passswort) {
                                    URLSession.shared.dataTask(with: url) { data, response, error in
                                        if let data = data {
                                            do {
                                                let benutzer = try JSONDecoder().decode(User.self, from: data)
                                               print(benutzer)
                                                if(benutzer.id == "0"){
                                                    check = "Leider sind die Anmeldedaten falsch"
                                                }else{
                                                    DispatchQueue.main.async {
                                                        check=""
                                                        let window = UIApplication
                                                            .shared
                                                            .connectedScenes
                                                            .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                                                            .first { $0.isKeyWindow }
                                                        
                                                        window?.rootViewController = UIHostingController(rootView: Menü(b1:benutzer))
                                                        window?.makeKeyAndVisible()
                                                    }
                                                }
                                            } catch let error {
                                                check = "Leider sind die Anmeldedaten falsch"
                                                print(error)
                                            }
                                        }
                                    }.resume()
                                }
                            
                        })
                       {
                            Text("einloggen")
                                .fontWeight(.semibold)
                                .font(.system(size: 17))
                                .padding()
                                .foregroundColor(.white)
                                .background(Color.black)
                                .cornerRadius(40)
                            }
                        Spacer()
                    }
                    .padding(.top,30)
                }
                Spacer()
            }
        }
    }
}

extension LogIn{
    
    func login() {
        Auth0 // 1
            .webAuth() // 2
            .start { result in // 3
                switch result {
                    // 4
                case .failure(let error):
                    print("Failed with: \(error)")
                    // 5
                case .success(let credentials):
                    userProfile = Profile.from(credentials.idToken)
                    print("Credentials: \(credentials)")
                    print("ID token: \(credentials.idToken)")
                    print("hurraaaaa")
                    let window = UIApplication
                                .shared
                                .connectedScenes
                                .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                                .first { $0.isKeyWindow }
                    window?.rootViewController = UIHostingController(rootView: Menü(b1: User(id: "1", name: "name", username: "benutzername", password: "passwort", admin: false)))
                            window?.makeKeyAndVisible()
                }
            }
    }
    
    func logout() {
        Auth0 // 1
            .webAuth() // 2
            .clearSession { result in // 3
                switch result {
                    // 4
                case .failure(let error):
                    print("Failed with: \(error)")
                    // 5
                case .success:
                    print("ausgeloggt")
                }
            }
    }
}
struct LogIn_Previews: PreviewProvider {
    static var previews: some View {
        LogIn()
    }
}
