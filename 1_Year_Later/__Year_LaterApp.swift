//
//  __Year_LaterApp.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 03.12.22.
//

import SwiftUI

@main
struct __Year_LaterApp: App {
    
    var body: some Scene {
        WindowGroup {
            LogIn()
        }
    }
}

