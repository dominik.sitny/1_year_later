//
//  Benutzer.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 03.12.22.
//

import Foundation

struct Group: Identifiable, Codable {
       
    var id: String
    var name: String
    var memberList: [User]
    var erstellungsdatum:Date
    
}


