//
//  Benutzer.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 27.01.23.
//

import Foundation

struct User: Identifiable, Codable {
    
    var id: String
    var name:String
    var username:String
    var password:String?
    var admin:Bool
    var friendslist:[String]?
    var state:[Int]?
}


