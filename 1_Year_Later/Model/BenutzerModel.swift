//
//  BenutzerModel.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 27.01.23.
//

import Foundation

class BenutzerModel: ObservableObject {

    @Published var user: User = User(id: "", name: "", username: "", admin: false)
    @Published var alleBenutzer: [User] = []
    @Published var alleBenutzerMocks: [User] = []
    @Published var alleFriends:[User] = []
    var searchedUsers:[User] = []
    @Published var allUsernames:[String]=[]
    var found:Bool = Bool()
    
    init(){
        
        
    }

    /**
            Hier werden die Informationen aus bestimmten Usern mithilfe eines getRequest geforder
     */
    func reload(url:String) {
        let url = URL(string: url)!
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: url) { data, _, error in
            // Error handling in case the data couldn't be loaded
            // For now, only display the error on the console
            guard let data = data else {
                debugPrint("Error loading \(url): \(String(describing: error))")
                return
            }
            // Parse JSON with JSONDecoder assuming valid JSON data
            let alleBenutzer = try! JSONDecoder().decode([User].self, from: data)
            if(alleBenutzer.count==0 || alleBenutzer[0].id == "0"){
                
            }
            else{
                self.found = true
            }
            // Update UI
            OperationQueue.main.addOperation {
                self.alleBenutzer = alleBenutzer
                
            }
        }

        task.resume()
    }
    
    /**
        Hier werden die Freunde des Users asynchron geladen
     */
    func getFriends(id:String) async{
        let url = URL(string: Configs.init().restAdresse+"/getFriends?id="+id)!
            let urlSession = URLSession.shared
            let task = urlSession.dataTask(with: url) { data, _, error in
                // Error handling in case the data couldn't be loaded
                // For now, only display the error on the console
                guard let data = data else {
                    debugPrint("Error loading \(url): \(String(describing: error))")
                    return
                }
                // Parse JSON with JSONDecoder assuming valid JSON data
                var alleFriends = try! JSONDecoder().decode([User].self, from: data)
                if alleFriends.count==0{
                    alleFriends = []
                }
                // Update UI
                OperationQueue.main.addOperation {
                    self.alleFriends = alleFriends
                }
            }
            task.resume()
        }
    
    /**
     Hier werden die Freunde des Users synchron geladen
     */
    func getFriends2(id:String){
        let url = URL(string: Configs.init().restAdresse+"/getFriends?id="+id)!
            let urlSession = URLSession.shared
            let task = urlSession.dataTask(with: url) { data, _, error in
                // Error handling in case the data couldn't be loaded
                // For now, only display the error on the console
                guard let data = data else {
                    debugPrint("Error loading \(url): \(String(describing: error))")
                    return
                }
                // Parse JSON with JSONDecoder assuming valid JSON data
                var alleFriends = try! JSONDecoder().decode([User].self, from: data)
                if alleFriends.count==0{
                    alleFriends = []
                }
                
              
                // Update UI
                OperationQueue.main.addOperation {
                    self.alleFriends = alleFriends
                    
                    
                }
            }

            task.resume()
        }
    
    /**
        Hier werden die User geladen, um die Liste nach gesuchten Usern zu füllen
     */
    func getSearchedUsers(name:String) -> [User]{
        let name = name.replacingOccurrences(of: " ", with: "%20")
        let url = URL(string: Configs.init().restAdresse+"/getSearchedUsernames?username="+name)!
            let urlSession = URLSession.shared
            let task = urlSession.dataTask(with: url) { data, _, error in
                
                print(data as Any)
                
                // Error handling in case the data couldn't be loaded
                // For now, only display the error on the console
                guard let data = data else {
                    debugPrint("Error loading \(url): \(String(describing: error))")
                    return
                }
                // Parse JSON with JSONDecoder assuming valid JSON data
                let alleFriends = try! JSONDecoder().decode([User].self, from: data)

                if(alleFriends.count==0 || alleFriends[0].id=="0" ){
                    self.searchedUsers = []
                }
                else{
                    self.searchedUsers = alleFriends

                }
            }

            task.resume()
        return searchedUsers
        }
    
    /**
            Für die Registrierung wird geprüft, ob der eingegebene Username schon existiert. Daher werden hier alle Name geladen
     */
    func getUsernames(url:String) {
        let url = URL(string: url)!
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: url) { data, _, error in
            // Error handling in case the data couldn't be loaded
            // For now, only display the error on the console
            guard let data = data else {
                debugPrint("Error loading \(url): \(String(describing: error))")
                return
            }
            // Parse JSON with JSONDecoder assuming valid JSON data
            let alleBenutzer = try! JSONDecoder().decode([String].self, from: data)
            if(alleBenutzer.count==0){
                
            }
            else{
                self.allUsernames = alleBenutzer
            }
            // Update UI
            OperationQueue.main.addOperation {
                self.allUsernames = alleBenutzer
                
            }
        }
        task.resume()
    }
}



