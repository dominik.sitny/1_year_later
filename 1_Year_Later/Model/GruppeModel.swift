//
//  GruppeModel.swift
//  1_Year_Later
//
//  Created by Dominik Sitny on 06.02.23.
//

import Foundation

class GruppeModel: ObservableObject {
    
    @Published var alleGruppen: [Group] = []
    
    init(){
        
    }
    
    /**
        Alle Gruppen des Users werden geladen
     */
    func getGroups(id:String, group:Bool){
        
        var url:URL
        
        if group {
            url = URL(string: Configs.init().restAdresse+"/getGroups?id="+id+"&group=true")!
        }else{
            url = URL(string: Configs.init().restAdresse+"/getGroups?id="+id+"&group=false")!
        }
            let urlSession = URLSession.shared
            let task = urlSession.dataTask(with: url) { data, _, error in
                // Error handling in case the data couldn't be loaded
                // For now, only display the error on the console
                guard let data = data else {
                    debugPrint("Error loading \(url): \(String(describing: error))")
                    return
                }
                
                let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(self.dateFormatter)
                    let alleGruppen = try! decoder.decode([Group].self, from: data)
              
                // Update UI
                OperationQueue.main.addOperation {
                    self.alleGruppen = alleGruppen
                    
                }
            }

            task.resume()
        }
        
    /**
        Das Datum wird passend formatiert. Diese Formatierung nutzen die Date-Objekte einer NoSql-Datenbank
     */
    private let dateFormatter: DateFormatter = {
            let df = DateFormatter()
            df.locale = Locale(identifier: "en_US_POSIX")
            df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            return df
            }()
    
}
